---
date: 2023-09-05
description: "Enjoying the Startup Founder Ride"
featured_image: "/images/chill.jpg"
tags: ["blog"]
title: "Enjoying the Startup Founder Ride"
---

"Starting a company is like chewing glass while staring into the abyss". This quote has been apocryphally attributed to Elon Musk but I'm pretty sure I came across it before Elon entered the public consciousness. . It might also be a paraphrase of Sean Parkers line "Starting a company is like chewing glass. Eventually you start to like the taste of your own blood".
It paints entrepreneurship as a painful endeavor. While starting a company is certainly challenging, it doesn't have to be sheer misery.

## Manage your personal risk

Failure is not an option is not the right mindset. Failure is always an option. In fact, it's the most likely outcome. Most startups fail. That's the nature of the game. If you're not prepared to fail, you're not prepared to start a company. That way madness lies. Your startup failing should not lead to personal disaster. 

## Focus on the product

Your idea is either not unique or it is probably a bad idea. Don't get stressed about being beaten to market. Focus on building the right product. The early bird gets the worm but the second mouse gets the cheese.

## Surround yourself with the right people

If you can, make sure you have access to people who have done it before. They can help you avoid common pitfalls and give you a reality check when you need it. 

## Enjoy the journey

These may be the most exhilarating years of your life if you embrace them fully while building your company. Nothing matters very much and very few things matter at all. It's all upside. You're building something new. You're creating value. You're making a difference. You're learning. You're growing. You're meeting interesting people. You're having fun.

